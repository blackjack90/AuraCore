package academy.softserve.aura.core.swagger.config;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import io.swagger.configuration.RFC3339DateFormat;
import io.swagger.configuration.SwaggerDocumentationConfig;
import io.swagger.configuration.SwaggerUiConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@ComponentScan(basePackages = "academy.softserve.aura.core.swagger.controllers")
@EnableWebMvc
@EnableSwagger2 //Loads the spring beans required by the framework
@PropertySource("classpath:swagger.properties")
@Import(SwaggerDocumentationConfig.class)
public class XmlConverterConfig extends SwaggerUiConfiguration {


    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.xml()
                .annotationIntrospector(new CustomAnnotationIntrospector())
                .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
                .dateFormat( new RFC3339DateFormat())
                .build();

        converters.add(new MappingJackson2XmlHttpMessageConverter(objectMapper));

        super.configureMessageConverters(converters);
    }

    /*
        Custom Annotation introspector overrides method that is used
        for finding rootName and setting is as desired.
     */
    public class CustomAnnotationIntrospector extends AnnotationIntrospector {


        @Override
        public PropertyName findRootName(AnnotatedClass ac){
            String annotatedClass = ac.getAnnotated().getSimpleName();
            if (annotatedClass.contains("Dto")) {
                return PropertyName.construct(annotatedClass.replace("Dto",""));
            } else {
                return null;
            }
        }

        @Override
        public Version version() {
            return Version.unknownVersion();
        }
    }
}
