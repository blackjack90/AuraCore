package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.Item;

import java.util.Collection;

/**
 * The interface which extends basic {@link CrudDao} to work with {@link CommonItem}
 * by adding custom methods.
 *
 * @see Item
 * @see CommonItem
 *
 * @author Ivan Vakhovskyi
 */
public interface CommonItemDao extends CrudDao<CommonItem> {

    /**
     * Gets certain number of {@link CommonItem} from collection by their working status.
     *
     * @param offset    the first entry to return from a collection
     * @param limit     the quantity of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<CommonItem> findFewByIsWorking(int offset, int limit, boolean isWorking);

    /**
     * Gets all {@link Department} entities which used specific {@link CommonItem}.
     *
     * @param itemID the id of {@link CommonItem}
     * @return Collection ({@link java.util.List}) of {@link Department} which used specific {@link CommonItem}
     */
    Collection<Department> getCommonItemDepartments(Long itemID);

    /**
     * Gets number of {@link CommonItem} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getCommonItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Gets {@link CommonItem} and change it to {@link Item}
     *
     * @param commonItem what will be converted to {@link Item}
     * @return true if CommonItem was assign to Warehouse
     */
    boolean assignCommonItemToWarehouse (CommonItem commonItem);

}