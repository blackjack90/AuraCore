package academy.softserve.aura.core.dao;


import academy.softserve.aura.core.entity.Setting;

public interface SettingDao extends CrudDao<Setting>{

}
