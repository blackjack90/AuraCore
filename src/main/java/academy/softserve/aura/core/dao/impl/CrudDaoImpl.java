package academy.softserve.aura.core.dao.impl;

import academy.softserve.aura.core.dao.CrudDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;

@Repository
public abstract class CrudDaoImpl<E> implements CrudDao<E> {

    @Autowired
    protected SessionFactory sessionFactory;
    private Class<E> elementClass;

    public CrudDaoImpl(Class<E> elementClass) {
        this.elementClass = elementClass;
    }
    @Override
    public E addElement(E element) {
        Session session = sessionFactory.getCurrentSession();
        session.save(element);
        return element;
    }

    @Override
    public Collection<E> addAll(Collection<E> elements) {
        Session session = sessionFactory.getCurrentSession();
        for (E element : elements) {
            session.save(element);
        }
        return elements;
    }

    @Override
    public E updateElement(E element) {
        Session session = sessionFactory.getCurrentSession();
        session.merge(element);
        return element;
    }

    @Override
    public E getElementByID(Long elementId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(elementClass, elementId);
    }

    @Override
    public Collection<E> getAllElements() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<E> criteria = builder.createQuery(elementClass);
        Root<E> elementRoot = criteria.from(elementClass);
        criteria.select(elementRoot);
        return session.createQuery(criteria).getResultList();
    }

    @Override
    public boolean deleteElement(Long elementId) {
        Session session = sessionFactory.getCurrentSession();
        E element = getElementByID(elementId);
        if (element == null) {
            return false;
        }
        session.delete(element);
        return true;
    }

    @Override
    public Collection<E> getFew(int offSet, int limit) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from " + elementClass.getName(), elementClass)
                .setFirstResult(offSet)
                .setMaxResults(limit)
                .getResultList();
    }

    @Override
    public Long getRecordsCount() {
        Long elementCount;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<E> elementRoot = criteriaQuery.from(elementClass);

        criteriaQuery.select(builder.count(elementRoot));
        elementCount = session.createQuery(criteriaQuery).getSingleResult();

        return elementCount;
    }

}
