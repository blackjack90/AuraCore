package academy.softserve.aura.core.dao;

import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;

import java.util.Collection;

/**
 * The interface which extends basic {@link CrudDao} to work with {@link PrivateItem}
 * by adding custom methods.
 *
 * @see Item
 * @see PrivateItem
 *
 * @author Ivan Vakhovskyi
 */
public interface PrivateItemDao extends CrudDao<PrivateItem> {

    /**
     * Gets certain number of {@link PrivateItem} from collection by their working status.
     *
     * @param offset the point of first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<PrivateItem> findFewByIsWorking(int offset, int limit, boolean isWorking);

    /**
     * Gets all {@link User} entities who used specific {@link PrivateItem}.
     *
     * @param itemID the id of {@link PrivateItem}
     * @return Collection ({@link java.util.List}) of {@link User} who used specific {@link PrivateItem}
     */
    Collection<User> getPrivateItemUsers(Long itemID);

    /**
     * Gets number of {@link PrivateItem} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getPrivateItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Gets {@link PrivateItem} and change it to {@link Item}
     *
     * @param privateItem what will be converted to {@link Item}
     * @return true if Private item was assign to Warehouse
     */
    boolean assignPrivateItemToWarehouse (PrivateItem privateItem);

}
