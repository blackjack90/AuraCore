package academy.softserve.aura.core.security.jwt;

import academy.softserve.aura.core.exceptions.handlers.GlobalAuraExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static academy.softserve.aura.core.utils.Constants.JWT_TOKEN_COOKIE_NAME;
import static academy.softserve.aura.core.utils.Constants.SIGNIN_KEY;

@Component
public class JwtFilter extends OncePerRequestFilter {

    @Autowired
    private GlobalAuraExceptionHandler globalAuraExceptionHandler;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            UserDetails userDetails = JwtUtil.getUserDetails(httpServletRequest, JWT_TOKEN_COOKIE_NAME, SIGNIN_KEY);
            if (userDetails == null) {
                String pathInfo = httpServletRequest.getPathInfo();
                if (!pathInfo.endsWith("login")) {
                    SecurityContextHolder.getContext().setAuthentication(null);
                    globalAuraExceptionHandler.handleAuthenticationException(
                            new CredentialsExpiredException("You need to authorize yourself"),
                            httpServletRequest,
                            httpServletResponse);
                }
            } else {
                String token = CookieUtil.getValue(httpServletRequest, JWT_TOKEN_COOKIE_NAME);
                if (SecurityContextHolder.getContext().getAuthentication() == null
                        || !SecurityContextHolder.getContext().getAuthentication().getName().equals(userDetails.getUsername())) {
                    SecurityContextHolder.getContext().setAuthentication(new JwtTokenAuthentication(token, true, userDetails));
                }
                httpServletRequest.setAttribute("userDetails", userDetails);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            CookieUtil.clear(httpServletResponse, JWT_TOKEN_COOKIE_NAME);
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
