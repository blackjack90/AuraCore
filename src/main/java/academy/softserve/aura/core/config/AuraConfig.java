package academy.softserve.aura.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
@ImportResource("classpath:/AuraConfig.xml")
@ComponentScan(basePackages = {"academy.softserve.aura.core"})
public class AuraConfig {

    /**
     * Method validation post processor. This bean is created to apply the JSR validation in method
     * parameters. Any class which wants to perform method param validation must use @Validated
     * annotation at class level.
     *
     * Proxying must be set to class in order to make validation work.
     * This is done because controller implements an interface and annotation @Validated is not seen on controller class
     * when using JDK dynamic proxy.
     *
     * @return the method validation post processor
     */
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
        methodValidationPostProcessor.setProxyTargetClass(true);
        return methodValidationPostProcessor;
    }
}
