package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Address;
import academy.softserve.aura.core.entity.Contacts;
import academy.softserve.aura.core.entity.Email;
import academy.softserve.aura.core.entity.Phone;
import academy.softserve.aura.core.swagger.model.AddressDto;
import academy.softserve.aura.core.swagger.model.ContactsDto;
import academy.softserve.aura.core.swagger.model.EmailDto;
import academy.softserve.aura.core.swagger.model.PhoneDto;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link Contacts} entities and {@link ContactsDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 * Uses three {@link ListMapper} of specific types.
 *
 * @see Contacts
 */
@Component
public class ContactsMapper implements DtoMapper<ContactsDto, Contacts> {

    private final ListMapper<AddressDto, Address> addressListMapper;
    private final ListMapper<PhoneDto, Phone> phoneListMapper;
    private final ListMapper<EmailDto, Email> emailListMapper;

    /**
     * Constructor
     * Initializes ListMapper
     */
    public ContactsMapper() {
        this.addressListMapper = new ListMapper<>(new AddressMapper());
        this.phoneListMapper = new ListMapper<>(new PhoneMapper());
        this.emailListMapper = new ListMapper<>(new EmailMapper());
    }

    /**
     * Gets {@link Contacts} entity and maps them to specific DTO: {@link ContactsDto}.
     *
     * @param entity the the entity {@link Contacts} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ContactsDto}
     */
    @Override
    public ContactsDto toDto(Contacts entity) {
        if (entity == null) {
            return null;
        }

        ContactsDto contactsDto = new ContactsDto();

        contactsDto.setId(entity.getId());
        contactsDto.setEmails(emailListMapper.toDtoList(entity.getEmails()));
        contactsDto.setPhones(phoneListMapper.toDtoList(entity.getPhones()));
        contactsDto.setAddresses(addressListMapper.toDtoList(entity.getAddresses()));

        return contactsDto;
    }
    /**
     * Gets {@link ContactsDto} DTO object and maps them to specific entity: {@link Contacts}.
     *
     * @param dto the DTO {@link ContactsDto} type to be mapped to DTO object
     * @return new entity object of type {@link Contacts}
     */
    @Override
    public Contacts toEntity(ContactsDto dto) {
        if (dto == null) {
            return null;
        }

        Contacts contacts = new Contacts();

        if (dto.getId() != null){
            contacts.setId(dto.getId());
        }
        contacts.setEmails(emailListMapper.toEntityList(dto.getEmails()));
        contacts.setPhones(phoneListMapper.toEntityList(dto.getPhones()));
        contacts.setAddresses(addressListMapper.toEntityList(dto.getAddresses()));

        return contacts;
    }
}
