package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.model.UserDto;
import academy.softserve.aura.core.swagger.model.UserWithCredentialsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link User} entities and {@link UserDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see User
 */
@Component
public class UserMapper implements DtoMapper<UserDto, User> {

    private final ContactsMapper contactsMapper;

    @Autowired
    public UserMapper(ContactsMapper contactsMapper) {
        this.contactsMapper = contactsMapper;
    }

    /**
     * Gets {@link User} entity and maps them to specific DTO: {@link UserDto}.
     *
     * @param entity the the entity {@link User} type to be mapped to DTO object
     * @return mapped DTO object of type {@link UserDto}
     */
    @Override
    public UserDto toDto(User entity) {
        if (entity == null) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setIsActive(entity.isActive());
        Long id = entityDepartmentId(entity);
        if (id != null) {
            userDto.setDepartmentId(id);
        }
        userDto.setId(entity.getId());
        userDto.setFirstName(entity.getFirstName());
        userDto.setLastName(entity.getLastName());
        userDto.setContacts(contactsMapper.toDto(entity.getContacts()));

        return userDto;
    }

    /**
     * @param dto incoming Entity object to map
     * @throws AuraMapperIllegalMethodException
     * @deprecated Must not be used because no entity objects of type {@link User} must be made from {@link UserDto}
     * Use {@link UserWithCredentialsMapper#toEntity(UserWithCredentialsDto)} instead.
     */
    @Deprecated
    @Override
    public User toEntity(UserDto dto) {
        throw new AuraMapperIllegalMethodException("This method is unsupported for current object");
    }

    /**
     * Gets ID of department user belongs to
     * @param user {@link User} for which department id is returned
     * @return id of user's department
     */
    private static Long entityDepartmentId(User user) {
        Department department = user.getDepartment();

        if (department == null || department.getId() == null) {
            return null;
        } else {
            return department.getId();
        }
    }
}

