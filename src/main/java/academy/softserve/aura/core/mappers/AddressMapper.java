package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Address;
import academy.softserve.aura.core.swagger.model.AddressDto;
import org.springframework.stereotype.Component;

/**
 * The mapper class to perform mapping between
 * {@link Address} entities and {@link AddressDto} DTO objects.
 * It implements basic {@link DtoMapper}.
 *
 * @see Address
 */
@Component
public class AddressMapper implements DtoMapper<AddressDto, Address> {

    /**
     * Gets {@link Address} entity and maps them to specific DTO: {@link AddressDto}.
     *
     * @param entity the the entity {@link Address} type to be mapped to DTO object
     * @return mapped DTO object of type {@link AddressDto}
     */
    @Override
    public AddressDto toDto(Address entity) {
        if (entity == null) {
            return null;
        }

        AddressDto addressDto = new AddressDto();

        addressDto.setId(entity.getId());
        addressDto.setFullAddress(entity.getFullAddress());

        return addressDto;
    }

    /**
     * Gets {@link AddressDto} DTO object and maps them to specific entity: {@link Address}.
     *
     * @param dto the DTO {@link AddressDto} type to be mapped to DTO object
     * @return new entity object of type {@link Address}
     */
    @Override
    public Address toEntity(AddressDto dto) {
        if (dto == null) {
            return null;
        }

        Address address = new Address();

        address.setId(dto.getId());
        address.setFullAddress(dto.getFullAddress());

        return address;
    }
}
