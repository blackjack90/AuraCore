package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.model.UserWithCredentialsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserWithCredentialsMapper implements DtoMapper<UserWithCredentialsDto, User> {

    private final ContactsMapper contactsMapper;

    @Autowired
    public UserWithCredentialsMapper(ContactsMapper contactsMapper) {
        this.contactsMapper = contactsMapper;
    }

    /**
     * @param entity incoming Entity object to map
     * @throws AuraMapperIllegalMethodException
     * @deprecated Must not be used because no DTO objects of parameter type must be sent
     * Use {@link UserMapper#toDto(User)} instead.
     */
    @Deprecated
    @Override
    public UserWithCredentialsDto toDto(User entity) {
        throw new AuraMapperIllegalMethodException("This method is unsupported for current object");
    }

    @Override
    public User toEntity(UserWithCredentialsDto dto) {
        if (dto == null) {
            return null;
        }

        User user = new User();

        if (dto.getDepartmentId() == null) {
            user.setDepartment(null);
        } else {
            Department department = new Department();
            department.setId(dto.getDepartmentId());
            user.setDepartment(department);
        }

        if (dto.getIsActive() != null) {
            user.setActive(dto.getIsActive());
        } else {
            user.setActive(false);
        }

        user.setId(dto.getId());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setContacts(contactsMapper.toEntity(dto.getContacts()));
        user.setLogin(dto.getLogin());
        user.setPassword(dto.getPassword());

        if (dto.getRole() != null) {
            user.setUserRole(Enum.valueOf(UserRole.class, dto.getRole().toString()));
        }

        return user;
    }
}
