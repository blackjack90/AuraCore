package academy.softserve.aura.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "item_id", referencedColumnName = "id")
@Table(name = "private_item")
public class PrivateItem extends Item{

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User owner;

    @JsonIgnore
    @ManyToMany(mappedBy = "allItemsUsedByClients")
    private List<User> allUsersWhoUsedItem;

    @Column(name = "usage_start_date")
    private OffsetDateTime usageStartDate;

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getAllUsersWhoUsedItem() {
        return allUsersWhoUsedItem;
    }

    public void setAllUsersWhoUsedItem(List<User> allUsersWhoUsedItem) {
        this.allUsersWhoUsedItem = allUsersWhoUsedItem;
    }

    public OffsetDateTime getUsageStartDate() {
        return usageStartDate;
    }

    public void setUsageStartDate(OffsetDateTime usageStartDate) {
        this.usageStartDate = usageStartDate;
    }
}
