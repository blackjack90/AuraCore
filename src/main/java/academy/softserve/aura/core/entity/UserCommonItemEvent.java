package academy.softserve.aura.core.entity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "user_common_item_event")
public class UserCommonItemEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(name = "common_item_item_id", nullable = false)
    private Long commonItemId;

    @Column(name = "usage_start_date", nullable = false)
    private OffsetDateTime usageStartDate;

    @Column(name = "usage_end_date", nullable = false)
    private OffsetDateTime usageEndDate;

    @Column(name = "usage_duration")
    private double usageDuration;

    @Transient
    private CommonItem commonItem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCommonItemId() {
        return commonItemId;
    }

    public void setCommonItemId(Long commonItemId) {
        this.commonItemId = commonItemId;
    }

    public OffsetDateTime getUsageStartDate() {
        return usageStartDate;
    }

    public void setUsageStartDate(OffsetDateTime usageStartDate) {
        this.usageStartDate = usageStartDate;
    }

    public OffsetDateTime getUsageEndDate() {
        return usageEndDate;
    }

    public void setUsageEndDate(OffsetDateTime usageEndDate) {
        this.usageEndDate = usageEndDate;
    }

    public double getUsageDuration() {
        return usageDuration;
    }

    public void setUsageDuration(double usageDuration) {
        this.usageDuration = usageDuration;
    }

    public CommonItem getCommonItem() {
        return commonItem;
    }

    public void setCommonItem(CommonItem commonItem) {
        this.commonItem = commonItem;
    }
}
