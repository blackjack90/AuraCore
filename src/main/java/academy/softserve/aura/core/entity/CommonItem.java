package academy.softserve.aura.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn(name = "item_id", referencedColumnName = "id")
@Table(name = "common_item")
public class CommonItem extends Item {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id", referencedColumnName = "id", nullable = false)
    private Department department;

    @JsonIgnore
    @ManyToMany(mappedBy = "allCommonItemsBelongToDepartment")
    private List<Department> allDepartmentsWhichOwnedCommonItem;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Department> getAllDepartmentsWhichOwnedCommonItem() {
        return allDepartmentsWhichOwnedCommonItem;
    }

    public void setAllDepartmentsWhichOwnedCommonItem(List<Department> allDepartmentsWhichOwnedCommonItem) {
        this.allDepartmentsWhichOwnedCommonItem = allDepartmentsWhichOwnedCommonItem;
    }
}
