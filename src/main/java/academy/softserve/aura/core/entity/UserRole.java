package academy.softserve.aura.core.entity;

public enum UserRole {
    ADMIN, MANAGER, USER
}
