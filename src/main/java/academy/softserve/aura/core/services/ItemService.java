package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.PrivateItem;

import java.util.Collection;

/**
 * The interface to work with {@link Item} entities.
 * It is extends basic {@link CrudService}.
 *
 * @see Item
 */
public interface ItemService extends CrudService<Item> {
    /**
     * Gets few {@link Item} from collection by their working status.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<Item> findFewByIsWorking(int offset, int limit, boolean isWorking);

    /**
     * Gets number of {@link Item} records in database.
     *
     * @return {@link Long} count of records in database.
     */
    Long getItemsCount();

    /**
     * Gets number of {@link Item} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Assign {@link Item} to User. Then Item can be used as {@link PrivateItem}
     *
     * @param item
     * @param userId id of User
     * @return boolean value if {@link PrivateItem} was created
     */
    boolean assignItemToUser(Item item, Long userId);

    /**
     * Assign {@link Item} to Department. Then Item can be used as {@link CommonItem}
     *
     * @param item
     * @param departmentId id of Department
     * @return boolean value if {@link CommonItem} was created
     */
    boolean assignItemToDepartment(Item item, Long departmentId);

}
