package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.entity.User;

import java.util.Collection;

/**
 * The interface to work with {@link PrivateItem} entities.
 * It is extends basic {@link CrudService}.
 *
 * @see Item
 * @see PrivateItem
 */
public interface PrivateItemService extends CrudService<PrivateItem> {

    /**
     * Gets few {@link PrivateItem} from collection by their working status.
     *
     * @param offset the first entry to return from a collection
     * @param limit  the quantity of entries to return from a collection
     * @param isWorking the status of Items to be fetched from a DB
     * @return Collection ({@link java.util.List}) of {@link Item} which satisfy parameters
     */
    Collection<PrivateItem> findFewByIsWorking(int offset, int limit, boolean isWorking);

    /**
     * Gets all {@link User} entities who used specific {@link PrivateItem}.
     *
     * @param id the id of {@link PrivateItem}
     * @return Collection ({@link java.util.List}) of {@link User} who used specific {@link PrivateItem}
     */
    Collection<User> getItemUsers(Long id);

    /**
     * Gets number of {@link PrivateItem} records in database.
     *
     * @return {@link Long} count of records in database.
     */
    Long getPrivateItemsCount();

    /**
     * Gets number of {@link PrivateItem} records in database filtered by working status.
     *
     * @param isWorking the status of Items to be fetched from a DB
     * @return {@link Long} count of records in database filtered by working status.
     */
    Long getPrivateItemsFilteredByIsWorkingCount(boolean isWorking);

    /**
     * Assign {@link PrivateItem} to Warehouse. Then Private Item can be used as {@link Item}
     *
     * @param privateItem
     * @return boolean value if {@link Item} was created
     */
    boolean assignPrivateItemToWarehouse (PrivateItem privateItem);

}
