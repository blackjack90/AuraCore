package academy.softserve.aura.core.services.impl;


import academy.softserve.aura.core.dao.DepartmentDao;
import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;
import academy.softserve.aura.core.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static academy.softserve.aura.core.services.impl.Utils.getUserLogin;
import static academy.softserve.aura.core.services.impl.Utils.isManager;


@Service
public class DepartmentServiceImpl extends CrudServiceImpl<Department> implements DepartmentService {

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional
    public Department create(Department entity) {
        Long entityId = entity.getId();

        if (entityId != null && departmentDao.getElementByID(entityId) != null) {
            return departmentDao.updateElement(entity);
        }
        return departmentDao.addElement(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Department findById(Long departmentId) {
        checkIfUserHasRights(departmentId);
        return departmentDao.getElementByID(departmentId);
    }

    @Override
    @Transactional
    public Department update(Department entity) {
        checkIfUserHasRights(entity.getId());

        Long entityId = entity.getId();

        if (entityId == null || departmentDao.getElementByID(entityId) == null) {
            return departmentDao.addElement(entity);
        }
        return departmentDao.updateElement(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<CommonItem> getDepartmentCommonItems(Long departmentId) {
        checkIfUserHasRights(departmentId);
        return departmentDao.getDepartmentCommonItems(departmentId);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<User> getDepartmentUsers(Long departmentId) {
        checkIfUserHasRights(departmentId);
        return departmentDao.getDepartmentUsers(departmentId);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getDepartmentCount() {
        return departmentDao.getRecordsCount();
    }

    @Override
    @Transactional(readOnly = true)
    public Long getDepartmentCommonItemsCount(Long departmentId) {
        checkIfUserHasRights(departmentId);
        return departmentDao.getDepartmentCommonItemsCount(departmentId);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getDepartmentUsersCount(Long departmentId) {
        checkIfUserHasRights(departmentId);
        return departmentDao.getDepartmentUsersCount(departmentId);
    }

    /**
     * Check whether manager has rights to access or modify information about department.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access
     * or change this information.
     *
     * @param departmentId id of department
     */
    private void checkIfUserHasRights(Long departmentId) {
        if (isManager()) {
            User user = userDao.getByLogin(getUserLogin());
            Long userDepartmentId = user.getDepartment().getId();

            if (userDepartmentId == null || !userDepartmentId.equals(departmentId)) {
                throw new AuraPermissionsException("Operation not permitted");
            }
        }
    }

}
