package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.CrudDao;
import academy.softserve.aura.core.services.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public class CrudServiceImpl<E> implements CrudService<E> {

    @Autowired
    private CrudDao<E> crudDao;

    @Override
    @Transactional
    public E create(E entity) {
        return crudDao.addElement(entity);
    }

    @Override
    @Transactional
    public Collection<E> createAll(Collection<E> entities) {
        return crudDao.addAll(entities);
    }

    @Override
    @Transactional
    public E findById(Long id) {
        return crudDao.getElementByID(id);
    }

    @Override
    @Transactional
    public Collection<E> findAll() {
        return crudDao.getAllElements();
    }

    @Override
    @Transactional
    public Collection<E> findFew(int offset, int limit) {
        return crudDao.getFew(offset, limit);
    }

    @Override
    @Transactional
    public E update(E entity) {
        return crudDao.updateElement(entity);
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        return crudDao.deleteElement(id);
    }

    @Override
    @Transactional
    public Long getRecordsCount() {
        return crudDao.getRecordsCount();
    }
}
