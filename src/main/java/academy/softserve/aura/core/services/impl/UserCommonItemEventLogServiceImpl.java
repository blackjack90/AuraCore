package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.dao.CommonItemDao;
import academy.softserve.aura.core.dao.UserCommonItemEventDao;
import academy.softserve.aura.core.dao.UserDao;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.exceptions.ItemNotFoundException;
import academy.softserve.aura.core.services.UserCommonItemEventLogService;
import academy.softserve.aura.core.services.messaging.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.Collection;

@Service
public class UserCommonItemEventLogServiceImpl extends CrudServiceImpl<UserCommonItemEvent>
        implements UserCommonItemEventLogService {

    @Autowired
    private UserCommonItemEventDao userCommonItemEventDao;
    @Autowired
    private CommonItemDao commonItemDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MessageSender messageSender;

    @Override
    @Transactional
    public UserCommonItemEvent create(UserCommonItemEvent entity) {
        Long itemId = entity.getCommonItemId();

        if (commonItemDao.getElementByID(itemId) == null) {
            throw new ItemNotFoundException("Common item with id " + itemId + " not found in database");
        }
        UserCommonItemEvent returnEntity = userCommonItemEventDao.addElement(entity);
        entity.setCommonItem(commonItemDao.getElementByID(itemId));
        entity.setId(returnEntity.getId());
        entity.setUser(userDao.getElementByID(returnEntity.getUser().getId()));
        messageSender.sendMessage(entity);

        return entity;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<UserCommonItemEvent> getAllSinceTime(OffsetDateTime sinceTime, int offset, int limit) {
        return userCommonItemEventDao.getAllSinceTime(sinceTime, offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<UserCommonItemEvent> getAllByUserId(Long id, int offset, int limit) {
        return userCommonItemEventDao.getAllByUserId(id, offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<UserCommonItemEvent> getAllByUserIdAndTime(Long id, OffsetDateTime sinceTime, int offset,
                                                                 int limit) {
        return userCommonItemEventDao.getAllByUserIdAndTime(id, sinceTime, offset, limit);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getAllSinceTimeCount(OffsetDateTime sinceTime) {
        return userCommonItemEventDao.getAllSinceTimeCount(sinceTime);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getAllByUserIdCount(Long id) {
        return userCommonItemEventDao.getAllByUserIdCount(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Long getAllByUserIdAndTimeCount(Long id, OffsetDateTime sinceTime) {
        return userCommonItemEventDao.getAllByUserIdAndTimeCount(id, sinceTime);
    }
}
