package academy.softserve.aura.core.services.impl;

import academy.softserve.aura.core.entity.Item;
import academy.softserve.aura.core.entity.ItemAmortization;
import academy.softserve.aura.core.entity.ItemAmortizationStatistic;
import academy.softserve.aura.core.services.FinancialService;
import academy.softserve.aura.core.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class FinancialServiceImpl implements FinancialService{
    static final int DAYS_IN_MONTH = 30;
    static final int HOURS_IN_DAY = 24;
    static final int MONTHS_IN_YEAR = 12;

    @Autowired
    private ItemService itemService;

    @Override
    @Transactional(readOnly = true)
    public List<ItemAmortization> getAmortizationReport(Integer offset, Integer limit) {

        List<Item> itemsList = (List<Item>) itemService.findFew(offset, limit);
        List<ItemAmortization> report = new ArrayList<>();
        for (Item current : itemsList){
            ItemAmortization itemAmortization = new ItemAmortization();

            int nominalResourceInHours = (int) current.getNominalResource();
            double nominalResourceInMonths = fromHoursToMonths(nominalResourceInHours);

            BigDecimal startPrice = current.getStartPrice();
            int totalMonthsOfUse = countMonthsOfUse(current.getManufactureDate());
            String firstMonthOfUse = getMonthAndYear(current.getManufactureDate(), 0);
            String lastMonthOfUse = getMonthAndYear(current.getManufactureDate(), nominalResourceInHours);
            BigDecimal valueOfMonthAmortization = monthlyAmortizationValue(current.getStartPrice(), nominalResourceInMonths);
            BigDecimal currentPrice = countCurrentPrice(totalMonthsOfUse, startPrice, valueOfMonthAmortization);

            itemAmortization.setId(current.getId());
            itemAmortization.setIsWorking(current.isWorking());
            itemAmortization.setManufacturer(current.getManufacturer());
            itemAmortization.setModel(current.getModel());
            itemAmortization.setStartPrice(startPrice);
            itemAmortization.setCurrentPrice(currentPrice);
            itemAmortization.setMonthOfUse(Long.valueOf(totalMonthsOfUse));
            itemAmortization.setFirstMonth(firstMonthOfUse);
            itemAmortization.setLastMonth(lastMonthOfUse);
            itemAmortization.setValueOfMonthAmortization(valueOfMonthAmortization);

            report.add(itemAmortization);
        }
        return report;
    }

    @Override
    @Transactional(readOnly = true)
    public ItemAmortization getAmortizationById(Long itemId) {
        Item item = itemService.findById(itemId);

        int nominalResourceInHours = (int) item.getNominalResource();
        double nominalResourceInMonths = fromHoursToMonths(nominalResourceInHours);

        ItemAmortization itemAmortization = new ItemAmortization();

        BigDecimal startPrice = item.getStartPrice();
        int monthsOfUse = countMonthsOfUse(item.getManufactureDate());
        String firstMonthOfUse = getMonthAndYear(item.getManufactureDate(), 0);
        String lastMonthOfUse = getMonthAndYear(item.getManufactureDate(), nominalResourceInHours);
        BigDecimal valueOfMonthAmortization = monthlyAmortizationValue(item.getStartPrice(), nominalResourceInMonths);
        BigDecimal currentPrice = countCurrentPrice(monthsOfUse, startPrice, valueOfMonthAmortization);

        itemAmortization.setId(item.getId());
        itemAmortization.setIsWorking(item.isWorking());
        itemAmortization.setManufacturer(item.getManufacturer());
        itemAmortization.setModel(item.getModel());
        itemAmortization.setStartPrice(startPrice);
        itemAmortization.setCurrentPrice(currentPrice);
        itemAmortization.setMonthOfUse(Long.valueOf(monthsOfUse));
        itemAmortization.setFirstMonth(firstMonthOfUse);
        itemAmortization.setLastMonth(lastMonthOfUse);
        itemAmortization.setValueOfMonthAmortization(valueOfMonthAmortization);

        return itemAmortization;
    }

    @Override
    public List<ItemAmortizationStatistic> getAmortizationStatisticByItemId(Long itemId, Integer offset, Integer limit) {
        Item item = itemService.findById(itemId);

        int nominalResourceInHours = (int) item.getNominalResource();
        double nominalResourceInMonths = fromHoursToMonths(nominalResourceInHours);

        List<ItemAmortizationStatistic> listForReturn = new ArrayList<>();

        BigDecimal startPrice = item.getStartPrice();
        BigDecimal valueOfMonthAmortization = monthlyAmortizationValue(item.getStartPrice(), nominalResourceInMonths);

        for (int i=offset; i<=(offset+limit); i++){
            ItemAmortizationStatistic current = new ItemAmortizationStatistic();
            current.setMonthOfUse(Long.valueOf(i));
            current.setValueOfMonthAmortization(valueOfMonthAmortization);

            BigDecimal currentPrice = countCurrentPrice(i, startPrice, valueOfMonthAmortization);
            if (currentPrice.compareTo(BigDecimal.ZERO) < 0){
                currentPrice = BigDecimal.ZERO;
            }

            current.setCurrentPrice(currentPrice);
            listForReturn.add(current);
        }
        return listForReturn;
    }

    private static String getMonthAndYear(OffsetDateTime offsetDateTime, int addHours){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM yyyy");
        OffsetDateTime time = offsetDateTime.plusHours(addHours);
        return time.format(formatter);
    }

    private static BigDecimal monthlyAmortizationValue (BigDecimal startPrice, double operationTimeInMonth){
        BigDecimal numberOfMonthForAmortization = BigDecimal.valueOf(operationTimeInMonth);

        return startPrice.divide(numberOfMonthForAmortization, 2, RoundingMode.CEILING);

    }

    private static int countMonthsOfUse (OffsetDateTime startUsageDate){
        OffsetDateTime now = OffsetDateTime.now();
        int yearOfNow = now.getYear();
        int monthsOfNow = now.getMonthValue();

        int yearOfStartUsageDate = startUsageDate.getYear();
        int monthsOfStartUsageDate = startUsageDate.getMonthValue();

        return (yearOfNow-yearOfStartUsageDate) * MONTHS_IN_YEAR + (monthsOfNow-monthsOfStartUsageDate);
    }

    private static BigDecimal countCurrentPrice (int monthsOfUse, BigDecimal startPrice, BigDecimal monthlyAmortization){
        BigDecimal months = new BigDecimal(monthsOfUse);
        BigDecimal amortizationDuringAllMonths = months.multiply(monthlyAmortization);
        return startPrice.subtract(amortizationDuringAllMonths);
    }

    private static double fromHoursToMonths (int hours){
        return (double) hours/HOURS_IN_DAY/DAYS_IN_MONTH;
    }
}