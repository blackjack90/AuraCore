package academy.softserve.aura.core.services;


import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraPermissionsException;

import java.util.Collection;

/**
 * The interface to work with {@link Department} entities.
 * It is extends basic {@link CrudService}.
 *
 * @see Department
 */
public interface DepartmentService extends CrudService<Department> {

    /**
     * Gets all {@link CommonItem} entities which belongs to specific {@link Department}.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access this information.
     *
     * @param id the id of {@link Department}
     * @return Collection ({@link java.util.List}) of CommonItems which belongs to specific {@link Department}
     */
    Collection<CommonItem> getDepartmentCommonItems(Long id);

    /**
     * Gets all {@link User} entities who works in specific {@link Department}.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access this information.
     *
     * @param id the id of {@link Department}
     * @return Collection ({@link java.util.List}) of {@link User} who works in specific {@link Department}
     */
    Collection<User> getDepartmentUsers(Long id);

    /**
     * Gets number of records in database.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access this information.
     *
     * @return {@link Long} count of records in database.
     */
    Long getDepartmentCount();

    /**
     * Gets number of {@link CommonItem} records in database that belong to {@link Department}.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access this information.
     *
     * @param id the id of {@link Department}
     * @return {@link Long} count of {@link CommonItem} records belonging to this department.
     */
    Long getDepartmentCommonItemsCount(Long id);

    /**
     * Gets number of {@link User} records in database that belong to {@link Department}.
     * Throws {@link AuraPermissionsException} in case if user does not have rights to access this information.
     *
     * @param id the id of {@link Department}
     * @return {@link Long} count of {@link User} records belonging to this department.
     */
    Long getDepartmentUsersCount(Long id);
}