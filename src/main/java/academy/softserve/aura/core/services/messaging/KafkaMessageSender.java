package academy.softserve.aura.core.services.messaging;


import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.exceptions.AuraException;
import academy.softserve.aura.core.services.SettingService;
import academy.softserve.aura.core.utils.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


@Service
public class KafkaMessageSender implements MessageSender {

    private static final Logger logger = LoggerFactory.getLogger(KafkaMessageSender.class);

    @Autowired
    private SettingService settingService;

    @Autowired
    private Properties kafkaProducerProperties;

    private ScheduledExecutorService singleThread = Executors.newSingleThreadScheduledExecutor();

    private KafkaProducer<String, String> producer;

    private Map<String, String> kafkaTopics = new HashMap<>();

    @PostConstruct
    public void startThread() {
        singleThread.scheduleWithFixedDelay(() -> {
            try {
                Properties settings = settingService.getSettings(Constants.SETTINGS_ID);

                if(settings != null) {
                    for (int i = 1;; i++) {
                        if (settings.getProperty("topic."+i) != null) {
                            kafkaTopics.put("topic."+i, settings.getProperty("topic."+i));
                        } else {
                            break;
                        }
                    }

                    if (producer == null) {
                        settings.stringPropertyNames().forEach(s -> kafkaProducerProperties.setProperty(s, settings.getProperty(s)));
                        producer = new KafkaProducer<>(kafkaProducerProperties);
                        logger.info("Producer initialized: {}", producer);
                    }
                } else if (producer != null) {
                    logger.info("Producer deactivated");
                    producer = null;
                } else {
                    logger.warn("No settings for producer creation in DB");
                }
            } catch (Exception e){
                logger.warn("I've got an exception: {}", e);
            }
        }, 0, Long.parseLong(kafkaProducerProperties.getProperty("delay", "5")), TimeUnit.SECONDS);
    }

    @Override
    public void sendMessage(Object message) {
        if (producer != null) {
            logger.info("going to send message: {}", message);
            ObjectMapper mapper = new ObjectMapper()
                    .registerModule(new JavaTimeModule())
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
            String jsonMessage;
            String topicName;
            try {
                jsonMessage = mapper.writeValueAsString(message);
            } catch (JsonProcessingException e) {
                logger.info("Error parsing message, {}", e.getLocalizedMessage());
                throw new AuraException("Mapping to Json failed in KafkaMessageSender");
            }
            if (message instanceof ItemLog) {
                topicName = kafkaTopics.get("topic.1");
                if (topicName != null) {
                    producer.send(new ProducerRecord<>(topicName, jsonMessage));
                }
                logger.info("ItemLog message sent successfully");
            }
            if (message instanceof UserCommonItemEvent) {
                topicName = kafkaTopics.get("topic.2");
                if (topicName != null) {
                    producer.send(new ProducerRecord<>(topicName, jsonMessage));
                }
                logger.info("UserCommonItemEvent message sent successfully");
            }
        } else {
            logger.warn(("Producer does not exist yet"));
        }
    }
}
