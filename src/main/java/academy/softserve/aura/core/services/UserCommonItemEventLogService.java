package academy.softserve.aura.core.services;

import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserCommonItemEvent;

import java.time.OffsetDateTime;
import java.util.Collection;

public interface UserCommonItemEventLogService extends CrudService<UserCommonItemEvent> {

    /**
     * SELECT operation for all event logs since some date.
     *
     * @param sinceTime start time
     * @param offset the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @return the collection of entities from DB
     */
    Collection<UserCommonItemEvent> getAllSinceTime(OffsetDateTime sinceTime, int offset, int limit);

    /**
     * SELECT operation for all event logs produced by user.
     *
     * @param id id of {@link User}
     * @param offset the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @return the collection of entities from DB
     */
    Collection<UserCommonItemEvent> getAllByUserId(Long id, int offset, int limit);

    /**
     * SELECT operation for all event logs produced by user since some date.
     *
     * @param id of the {@link User}
     * @param sinceTime start time
     * @param offset the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @return the collection of entities from DB
     */
    Collection<UserCommonItemEvent> getAllByUserIdAndTime(Long id, OffsetDateTime sinceTime, int offset, int limit);

    /**
     * Gets number of {@link UserCommonItemEvent} records in database from definite time.
     *
     * @param sinceTime start time
     * @return {@link Long} count of {@link UserCommonItemEvent} that satisfy query.
     */
    Long getAllSinceTimeCount(OffsetDateTime sinceTime);

    /**
     * Gets number of {@link UserCommonItemEvent} records in database from definite {@link User}.
     *
     * @param id of the {@link User}
     * @return {@link Long} count of {@link UserCommonItemEvent} that satisfy query.
     */
    Long getAllByUserIdCount(Long id);

    /**
     * Gets number of {@link UserCommonItemEvent} records in database from definite time and {@link User}.
     *
     * @param id of the {@link User}
     * @param sinceTime start time
     * @return {@link Long} count of {@link UserCommonItemEvent} that satisfy query.
     */
    Long getAllByUserIdAndTimeCount(Long id, OffsetDateTime sinceTime);

}
