package academy.softserve.aura.core.exceptions;

import academy.softserve.aura.core.entity.User;

/**
 *Thrown when an exceptional situation occurs while persisting {@link User} object.
 *
 *
 * @author  Eugene Savchenko
 */

public class AuraUserValidationException extends AuraUserException {

    /** Constructs a new {@code AuraUserValidationException} with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public AuraUserValidationException (){
        super();
    }

    /** Constructs a new {@code AuraUserValidationException} with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    public AuraUserValidationException(String message) {
        super(message);
    }
}
