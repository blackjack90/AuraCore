package academy.softserve.aura.core.swagger.controllers;

import academy.softserve.aura.core.entity.*;
import academy.softserve.aura.core.mappers.*;
import academy.softserve.aura.core.services.CommonItemService;
import academy.softserve.aura.core.services.ItemService;
import academy.softserve.aura.core.services.PrivateItemService;
import academy.softserve.aura.core.swagger.model.*;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ItemsApiControllerTest {

    @Mock
    private DtoMapper<CommonItemDto, CommonItem> commonItemDTOMapper = new CommonItemMapper();
    @Mock
    private DtoMapper<ItemDto, Item> itemDTOMapper = new ItemMapper();
    @Mock
    private DtoMapper<PrivateItemDto, PrivateItem> privateItemDTOMapper = new PrivateItemMapper();
    @Mock
    private DtoMapper<UserDto, User> userDTOMapper = new UserMapper(new ContactsMapper());
    @Mock
    private DtoMapper<DepartmentDto, Department> departmentDTOMapper;
    @Mock
    private CommonItemService commonItemService;
    @Mock
    private ItemService itemService;
    @Mock
    private PrivateItemService privateItemService;
    @InjectMocks
    private ItemsApiController itemsApiController;

    private PrivateItem privateItem = EntityFactory.createNotComplexPrivateItem();
    private PrivateItemDto privateItemDto = privateItemDTOMapper.toDto(privateItem);

    private Item item = EntityFactory.createNotComplexItem();
    private ItemDto itemDto = itemDTOMapper.toDto(item);

    private CommonItem commonItem = EntityFactory.createNotComplexCommonItem();
    private CommonItemDto commonItemDto = commonItemDTOMapper.toDto(commonItem);

    @Before
    public void setUp() {
        when(privateItemService.findById(anyLong())).thenReturn(privateItem);

        when(commonItemService.findById(anyLong())).thenReturn(commonItem);

        when(itemService.findById(anyLong())).thenReturn(item);
        when(itemService.assignItemToUser(item, item.getId())).thenReturn(true);

        when(privateItemDTOMapper.toDto(privateItem)).thenReturn(privateItemDto);
        when(privateItemDTOMapper.toEntity(privateItemDto)).thenReturn(privateItem);

        when(itemDTOMapper.toDto(item)).thenReturn(itemDto);
        when(itemDTOMapper.toEntity(itemDto)).thenReturn(item);

        when(commonItemDTOMapper.toDto(commonItem)).thenReturn(commonItemDto);
        when(commonItemDTOMapper.toEntity(commonItemDto)).thenReturn(commonItem);
    }


    @Test
    public void invokeUpdatePrivateItemByIdWithItemParameter(){
        privateItemDto.setItemType("Item");
        assertEquals(new ResponseEntity<>(privateItemDto, HttpStatus.OK),
                itemsApiController.updatePrivateItemById(privateItem.getId(), privateItemDto));

        Mockito.verify(itemService, Mockito.times(1)).assignItemToUser(eq(item), anyLong());
    }

    @Test
    public void invokeUpdateItemByIdWithPrivateItem(){
        itemDto.setItemType("PrivateItem");
        assertEquals(new ResponseEntity<>(itemDto, HttpStatus.OK),
                itemsApiController.updateItemById(item.getId(), itemDto));
        Mockito.verify(privateItemService, Mockito.times(1)).assignPrivateItemToWarehouse(eq(privateItem));
    }

    @Test
    public void invokeUpdateItemByIdWithCommonItem(){
        itemDto.setItemType("CommonItem");
        assertEquals(new ResponseEntity<>(itemDto, HttpStatus.OK),
                itemsApiController.updateItemById(item.getId(), itemDto));
        Mockito.verify(commonItemService, Mockito.times(1)).assignCommonItemToWarehouse(eq(commonItem));
    }

    @Test
    public void invokeUpdateCommonItemByIdWithItem(){
        commonItemDto.setItemType("Item");

        assertEquals(new ResponseEntity<>(commonItemDto, HttpStatus.OK),
                itemsApiController.updateCommonItemById(item.getId(), commonItemDto));
        Mockito.verify(itemService, Mockito.times(1)).assignItemToDepartment(eq(item), anyLong());

    }

    class IsPositiveLong extends ArgumentMatcher<Long> {
        public boolean matches(Object value) {
            return ((Long) value)> 0;
        }
    }
}
