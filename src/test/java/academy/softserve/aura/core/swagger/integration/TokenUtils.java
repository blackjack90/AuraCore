package academy.softserve.aura.core.swagger.integration;

import academy.softserve.aura.core.security.jwt.UserCredentialsInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.Date;
import java.util.HashMap;

import static academy.softserve.aura.core.utils.Constants.SIGNIN_KEY;

/**
 * for generation jwt tokens and putting it into context
 */
public class TokenUtils {

    private static ObjectMapper mapper = new MappingJackson2HttpMessageConverter().getObjectMapper();


    public static String generateToken(String subject, UserCredentialsInfo userDetails) {

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        JwtBuilder builder = Jwts.builder()
                .setSubject(subject)
                .setClaims(new HashMap<String, Object>() {{
                    try {
                        put("userDetails", mapper.writeValueAsString(userDetails));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }})
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, SIGNIN_KEY);
        return builder.compact();
    }



}
