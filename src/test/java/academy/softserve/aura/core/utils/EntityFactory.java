package academy.softserve.aura.core.utils;


import academy.softserve.aura.core.entity.*;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class EntityFactory {

    public static final String FIRST_NAME = "First name";
    public static final String LAST_NAME = "Last name";
    public static final String LOGIN = "login";
    public static final String PASS = "pass";
    public static final UserRole USER_ROLE = UserRole.ADMIN;
    public static final String GENERATED_DEPARTMENT_NAME = "Generated department";
    public static final boolean IS_ITEM_WORKING = true;
    public static final String ITEM_MANUFACTURER = "GENERATED MANUFACTURER";
    public static final String ITEM_MODEL = "GENERATED MODEL";
    public static final BigDecimal ITEM_START_PRICE = BigDecimal.valueOf(34.2546);
    public static final double ITEM_NOMINAL_RESOURCE = 2563.365;
    public static final String ITEM_DESCRIPTION = "GENERATED DESCRIPTION";
    public static final double ITEM_CURRENT_PRICE = 85858.36;
    public static final OffsetDateTime ITEM_USAGE_START_DATE = OffsetDateTime.of(2017, 7, 4, 12, 12, 12, 0, ZoneOffset.MAX);
    public static final OffsetDateTime FUTURE_DATE = OffsetDateTime.of(2117, 7, 4, 12, 12, 12, 0, ZoneOffset.MAX);
    public static final double AGING_FACTOR = 8.36;
    public static final String PHONE_NUMBER = "+3801234567";
    public static final String EMAIL = "email@email";
    public static final String ADDRESS = "Kyiv";
    public static final String KAFKA_TOPIC_NAME = "GENERATED KAFKA NAME";
    public static final String MICROSERVICE_HOST = "GENERATED MICROSERVICE HOST";
    public static final String KAFKA_HOST = "GENERATED KAFKA HOST";
    public static final boolean IS_ACTIVE = true;

    private static long counter = 1;

    //Constructor is private, because this factory must not be initialized
    private EntityFactory() {
    }

    public static Contacts createSimpleContacts() {
        Contacts contacts = new Contacts();

        Phone e = new Phone();
        e.setPhoneNumber(PHONE_NUMBER);
        List<Phone> phones = new ArrayList<>();
        phones.add(e);

        Email m = new Email();
        m.setEmailAddress(EMAIL);
        List<Email> emails = new ArrayList<>();
        emails.add(m);

        Address a = new Address();
        a.setFullAddress(ADDRESS);
        List<Address> addresses = new ArrayList<>();
        addresses.add(a);

        contacts.setPhones(phones);
        contacts.setAddresses(addresses);
        contacts.setEmails(emails);

        return contacts;
    }

    public static Department createEmptyDepartment() {
        Department department = new Department();
        department.setContacts(createSimpleContacts());
        department.setWorkers(new ArrayList<>());
        department.setCommonItems(new ArrayList<>());
        department.setName(GENERATED_DEPARTMENT_NAME);
        department.setId((long) (Math.random() * 100));
        return department;
    }

    public static User createUserWithCredentialsAndNewDepartment() {
        Department emptyDepartment = createEmptyDepartment();
        User user = new User();
        user.setActive(true);
        user.setId(System.nanoTime());
        user.setContacts(createSimpleContacts());
        user.setDepartment(emptyDepartment);
        emptyDepartment.getWorkers().add(user);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setUserItems(new ArrayList<>());
        user.setUserRole(USER_ROLE);
        user.setPassword(PASS);
        user.setLogin(LOGIN);
        return user;
    }

    public static List<Department> createEmptyDepartmentList(int size) {
        List<Department> departments = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            departments.add(createEmptyDepartment());
        }
        return departments;
    }

    public static User createUserWithCredentialsWithotDepartment() {
        User user = new User();
        user.setActive(true);
        user.setId(System.nanoTime());
        user.setContacts(createSimpleContacts());
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setUserItems(new ArrayList<>());
        user.setUserRole(USER_ROLE);
        user.setPassword(PASS);
        user.setLogin(LOGIN);
        return user;
    }

    public static Item createNotComplexItem() {
        Item item = new Item();
        item.setWorking(IS_ITEM_WORKING);
        item.setManufacturer(ITEM_MANUFACTURER);
        item.setModel(ITEM_MODEL);
        item.setStartPrice(ITEM_START_PRICE);
        item.setNominalResource(ITEM_NOMINAL_RESOURCE);
        item.setId(counter++);
        item.setDescription(ITEM_DESCRIPTION);
        item.setCurrentPrice(BigDecimal.valueOf(ITEM_CURRENT_PRICE));
        item.setComponents(new ArrayList<>());
        item.setManufactureDate(ITEM_USAGE_START_DATE);
        item.setAgingFactor(AGING_FACTOR);
        return item;
    }

    public static Item createComplexItem() {
        Item parent = createNotComplexItem();
        Item comp1 = createNotComplexItem();
        comp1.setParentItem(parent);
        Item comp2 = createNotComplexItem();
        comp2.setParentItem(parent);
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

    public static PrivateItem createNotComplexPrivateItem() {
        PrivateItem privateItem = new PrivateItem();
        privateItem.setOwner(createUserWithCredentialsAndNewDepartment());
        privateItem.setWorking(IS_ITEM_WORKING);
        privateItem.setManufacturer(ITEM_MANUFACTURER);
        privateItem.setModel(ITEM_MODEL);
        privateItem.setStartPrice(ITEM_START_PRICE);
        privateItem.setNominalResource(ITEM_NOMINAL_RESOURCE);
        privateItem.setId(counter++);
        privateItem.setDescription(ITEM_DESCRIPTION);
        privateItem.setCurrentPrice(BigDecimal.valueOf(ITEM_CURRENT_PRICE));
        privateItem.setComponents(new ArrayList<>());
        privateItem.setUsageStartDate(ITEM_USAGE_START_DATE);
        privateItem.setManufactureDate(ITEM_USAGE_START_DATE);
        return privateItem;
    }

    public static PrivateItem createComplexPrivateItem() {
        PrivateItem parent = createNotComplexPrivateItem();
        Item comp1 = createNotComplexPrivateItem();
        comp1.setParentItem(parent);
        Item comp2 = createNotComplexPrivateItem();
        comp2.setParentItem(parent);
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

    public static CommonItem createNotComplexCommonItem() {
        Department department = createEmptyDepartment();
        CommonItem commonItem = new CommonItem();
        commonItem.setDepartment(department);
        department.getCommonItems().add(commonItem);
        commonItem.setId(counter++);
        commonItem.setWorking(IS_ITEM_WORKING);
        commonItem.setManufacturer(ITEM_MANUFACTURER);
        commonItem.setModel(ITEM_MODEL);
        commonItem.setStartPrice(ITEM_START_PRICE);
        commonItem.setNominalResource(ITEM_NOMINAL_RESOURCE);
        commonItem.setDescription(ITEM_DESCRIPTION);
        commonItem.setCurrentPrice(BigDecimal.valueOf(ITEM_CURRENT_PRICE));
        commonItem.setComponents(new ArrayList<>());
        commonItem.setDepartment(department);
        commonItem.setManufactureDate(ITEM_USAGE_START_DATE);
        return commonItem;
    }

    public static CommonItem createComplexCommonItem() {
        CommonItem parent = createNotComplexCommonItem();
        CommonItem child1 = createNotComplexCommonItem();
        CommonItem child2 = createNotComplexCommonItem();
        child1.setParentItem(parent);
        child2.setParentItem(parent);
        parent.getComponents().add(child1);
        parent.getComponents().add(child2);
        return parent;
    }

    public static ItemLog createItemLog() {
        ItemLog itemLog = new ItemLog();
        itemLog.setId(System.nanoTime());
        itemLog.setDescription(ITEM_DESCRIPTION);
        itemLog.setEventDate(OffsetDateTime.now());
        itemLog.setEventType(ItemEventType.ITEM_CREATED);
        itemLog.setItemId(createNotComplexItem().getId().toString());
        return itemLog;
    }

    public static UserCommonItemEvent createUserCommonItemEvent() {
        UserCommonItemEvent event = new UserCommonItemEvent();
        event.setId(System.nanoTime());
        event.setCommonItemId(createNotComplexCommonItem().getId());
        event.setUser(createUserWithCredentialsAndNewDepartment());
        event.setUsageStartDate(ITEM_USAGE_START_DATE);
        event.setUsageEndDate(OffsetDateTime.now());
        return event;
    }

    public static Setting createSetting(){
        List<String> list = new ArrayList<>();
        list.add(KAFKA_TOPIC_NAME);
        Setting setting = new Setting();
        setting.setId(counter++);
        setting.setKafkaTopicNames(list);
        setting.setKafkaHost(KAFKA_HOST);
        setting.setMicroserviceHost(MICROSERVICE_HOST);
        setting.setActive(IS_ACTIVE);

        return setting;
    }

}
