package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Address;
import academy.softserve.aura.core.swagger.model.AddressDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class AddressMapperTest {
    @Autowired
    DtoMapper<AddressDto, Address> mapper;
    private Address address = new Address();
    private AddressDto dto = new AddressDto();

    @Before
    public void makeStubs() {
        address.setId(1L);
        address.setFullAddress("test_address1");
        dto.setFullAddress("test_address2");
        dto.setId(3L);
    }

    @Test
    public void toDto() throws Exception {
        dto = mapper.toDto(address);
        assertEquals(address.getId(), dto.getId());
        assertEquals(address.getFullAddress(), dto.getFullAddress());
        assertNull(mapper.toDto(null));
    }

    @Test
    public void toEntity() throws Exception {
        address = mapper.toEntity(dto);
        assertEquals(address.getId(), dto.getId());
        assertEquals(address.getFullAddress(), dto.getFullAddress());
        assertNull(mapper.toEntity(null));
    }

}