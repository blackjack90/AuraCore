package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Phone;
import academy.softserve.aura.core.swagger.model.PhoneDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class PhoneMapperTest {
    @Autowired
    DtoMapper<PhoneDto, Phone> mapper;
    private Phone phone = new Phone();
    private PhoneDto dto = new PhoneDto();

    @Before
    public void makeStubs() {
        phone.setId(1L);
        phone.setPhoneNumber("phone_toDto");
        dto.setPhoneNumber("phone_toEntity");
        dto.setId(3L);
    }

    @Test
    public void toDto() throws Exception {
        dto = mapper.toDto(phone);
        assertEquals(phone.getId(), dto.getId());
        assertEquals(phone.getPhoneNumber(), dto.getPhoneNumber());
        assertNull(mapper.toDto(null));
    }

    @Test
    public void toEntity() throws Exception {
        phone = mapper.toEntity(dto);
        assertEquals(phone.getId(), dto.getId());
        assertEquals(phone.getPhoneNumber(), dto.getPhoneNumber());
        assertNull(mapper.toEntity(null));
    }

}