package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.exceptions.AuraMapperIllegalMethodException;
import academy.softserve.aura.core.swagger.dto.DtoEntitiesFactory;
import academy.softserve.aura.core.swagger.model.UserWithCredentialsDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class UserWithCredentialsMapperTest {
    @Autowired
    UserWithCredentialsMapper mapper;

    @Test(expected = AuraMapperIllegalMethodException.class)
    public void toDto() throws Exception {
        UserWithCredentialsDto userDto = mapper.toDto(new User());
    }

    @Test
    public void toEntity() throws Exception {
        UserWithCredentialsDto userDto = DtoEntitiesFactory.createUserWithCredentialsDto();
        userDto.setDepartmentId(1L);
        User user = mapper.toEntity(userDto);

        assertEquals(user.getId(), userDto.getId());
        assertEquals(user.getFirstName(), userDto.getFirstName());
        assertEquals(user.getLastName(), userDto.getLastName());
        assertEquals(user.isActive(), userDto.getIsActive());
        assertEquals(user.getLogin(), userDto.getLogin());
        assertEquals(user.getPassword(), userDto.getPassword());
        assertEquals(user.getUserRole().toString(), userDto.getRole().toString());
        assertEquals(user.getDepartment().getId(), userDto.getDepartmentId());
        assertNotNull(user.getDepartment());
        assertNull(mapper.toEntity(null));

        userDto.setDepartmentId(null);
        userDto.setIsActive(null);
        userDto.setRole(null);
        user = mapper.toEntity(userDto);
        assertNull(user.getDepartment());
        assertNull(user.getUserRole());
        assertFalse(user.isActive());

    }

}