package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.swagger.dto.DtoEntitiesFactory;
import academy.softserve.aura.core.swagger.model.CommonItemDto;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class CommonItemMapperTest {
    @Autowired
    CommonItemMapper mapper;

    @Test
    public void toDto() {
        CommonItem item = EntityFactory.createComplexCommonItem();
        CommonItemDto mappedDto = mapper.toDto(item);
        assertTrue(mappedDto.getIsWorking());
        assertEquals(mappedDto.getId(), item.getId());
        assertEquals(mappedDto.getDepartmentId(), item.getDepartment().getId());
        assertEquals(mappedDto.getManufacturer(), item.getManufacturer());
        assertEquals(mappedDto.getModel(), item.getModel());
        assertEquals(mappedDto.getStartPrice(), item.getStartPrice());
        assertEquals(mappedDto.getCurrentPrice(), item.getCurrentPrice());
        assertTrue(mappedDto.getNominalResource() == item.getNominalResource());
        assertEquals(mappedDto.getDescription(), item.getDescription());
        assertNotNull(mappedDto.getComponents());
        assertEquals(mappedDto.getItemType(), item.getClass().getSimpleName());
        assertEquals(mappedDto.getManufactureDate(), item.getManufactureDate());
        assertTrue(mappedDto.getAgingFactor() == item.getAgingFactor());

        item.setComponents(null);
        assertNotNull(mapper.toDto(item).getComponents());

        item.setComponents(new ArrayList<>());
        assertNotNull(mapper.toDto(item).getComponents());
        assertTrue(mapper.toDto(item).getComponents().isEmpty());

        item.getDepartment().setId(null);
        assertNull(mapper.toDto(item).getDepartmentId());
        item.setDepartment(null);
        assertNull(mapper.toDto(item).getDepartmentId());

        item = null;
        assertNull(mapper.toDto((item)));
    }

    @Test
    public void toEntity() {
        CommonItemDto dto = DtoEntitiesFactory.createComplexCommonItemDto();
        CommonItem mappedItem = mapper.toEntity(dto);
        assertTrue(mappedItem.isWorking());
        assertEquals(mappedItem.getId(), dto.getId());
        assertEquals(dto.getDepartmentId(), mappedItem.getDepartment().getId());
        assertEquals(mappedItem.getManufacturer(), dto.getManufacturer());
        assertEquals(mappedItem.getModel(), dto.getModel());
        assertEquals(mappedItem.getStartPrice(), dto.getStartPrice());
        assertEquals(mappedItem.getCurrentPrice(), dto.getCurrentPrice());
        assertTrue(mappedItem.getNominalResource() == dto.getNominalResource());
        assertEquals(mappedItem.getDescription(), dto.getDescription());
        assertNotNull(mappedItem.getComponents());
        assertEquals(dto.getItemType(), mappedItem.getClass().getSimpleName());
        assertEquals(mappedItem.getManufactureDate(), dto.getManufactureDate());
        assertTrue(mappedItem.getAgingFactor() == dto.getAgingFactor());
        assertNotNull(mapper.toEntity(dto).getComponents().get(0).getParentItem());

        dto.setComponents(null);
        assertNotNull(mapper.toEntity(dto).getComponents());
        assertNull(mapper.toEntity(dto).getParentItem());

        dto.setComponents(new ArrayList<>());
        assertNotNull(mapper.toEntity(dto).getComponents());
        assertTrue(mapper.toEntity(dto).getComponents().isEmpty());

        dto.setIsWorking(null);
        assertFalse(mapper.toEntity(dto).isWorking());

        dto.setDepartmentId((null));
        assertNull(mapper.toEntity(dto).getDepartment());

        dto.setNominalResource(null);
        assertTrue(mapper.toEntity(dto).getNominalResource() == 0.0);


        dto = null;
        assertNull(mapper.toEntity((dto)));
    }

}