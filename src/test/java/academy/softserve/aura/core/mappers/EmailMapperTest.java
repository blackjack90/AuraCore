package academy.softserve.aura.core.mappers;

import academy.softserve.aura.core.entity.Email;
import academy.softserve.aura.core.swagger.model.EmailDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:AuraTestMappersConfig.xml")
public class EmailMapperTest {
    @Autowired
    DtoMapper<EmailDto, Email> mapper;
    private Email email = new Email();
    private EmailDto dto = new EmailDto();

    @Before
    public void makeStubs() {
        email.setId(1L);
        email.setEmailAddress("test@toDto");
        dto.setEmailAddress("test@toEntity");
        dto.setId(3L);
    }

    @Test
    public void toDto() throws Exception {
        dto = mapper.toDto(email);
        assertEquals(email.getId(), dto.getId());
        assertEquals(email.getEmailAddress(), dto.getEmailAddress());
        assertNull(mapper.toDto(null));
    }

    @Test
    public void toEntity() throws Exception {
        email = mapper.toEntity(dto);
        assertEquals(email.getId(), dto.getId());
        assertEquals(email.getEmailAddress(), dto.getEmailAddress());
        assertNull(mapper.toEntity(null));
    }

}