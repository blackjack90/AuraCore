package academy.softserve.aura.core.config.integration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
@ImportResource("classpath:/integration/AuraIntegrationConfig.xml")
public class AuraIntegrationConfig {

    /**
     * Method validation post processor. This bean is created to apply the JSR validation in method
     * parameters. Any class which wants to perform method param validation must use @Validated
     * annotation at class level.
     *
     * @return the method validation post processor
     */
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
        methodValidationPostProcessor.setProxyTargetClass(true);
        return methodValidationPostProcessor;
    }

}

