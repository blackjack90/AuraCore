package academy.softserve.aura.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan(basePackages = {"academy.softserve.aura.core"})
@ImportResource(locations = "classpath*:AuraConfigDaoTest.xml")
public class DaoConfig {

}
