package academy.softserve.aura.core.dao;


import academy.softserve.aura.core.entity.Department;
import academy.softserve.aura.core.entity.UserCommonItemEvent;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:daos/test_dao_application_context.xml")
public class UserCommonItemEventDaoImplTest {

    @Autowired
    private ItemDao itemDao;

    @Autowired
    private UserCommonItemEventDao userCommonItemEventDao;

    @Autowired
    private DepartmentDao departmentDao;

    @Autowired
    private UserDao userDao;

    private OffsetDateTime oldDate = OffsetDateTime.parse("2016-06-18T01:39:42.09+03:00");

    UserCommonItemEvent event1;
    UserCommonItemEvent event2;
    UserCommonItemEvent event3;
    UserCommonItemEvent testEvent;
    List<UserCommonItemEvent> list;
    int offset = 0;
    int limit = 10;

    @Before
    public void setUp() throws Exception {
        Department department = EntityFactory.createEmptyDepartment();

        event1 = EntityFactory.createUserCommonItemEvent();
        event2 = EntityFactory.createUserCommonItemEvent();
        event3 = EntityFactory.createUserCommonItemEvent();
        testEvent = EntityFactory.createUserCommonItemEvent();

        event1.getUser().setId(1L);
        event2.getUser().setId(2L);
        event3.getUser().setId(3L);

        event1.getUser().setDepartment(department);
        event2.getUser().setDepartment(department);
        event3.getUser().setDepartment(department);
        testEvent.getUser().setDepartment(department);

        departmentDao.addElement(department);

        userDao.addElement(event1.getUser());
        userDao.addElement(event2.getUser());
        userDao.addElement(event3.getUser());
        userDao.addElement(testEvent.getUser());

        list = new ArrayList<>();
        list.add(event2);
        list.add(event3);

        userCommonItemEventDao.addElement(event1);
        userCommonItemEventDao.addAll(list);
    }

    @Test
    @Transactional
    public void getAllTest() throws Exception {
        assertEquals(list.size()+1, userCommonItemEventDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void addElementTest() throws Exception {
        userCommonItemEventDao.addElement(testEvent);
        assertEquals(list.size() + 2, userCommonItemEventDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void addCollectionTest() throws Exception {
        List<UserCommonItemEvent> testList = new ArrayList<>();
        testList.add(testEvent);
        userCommonItemEventDao.addAll(testList);
        assertEquals(list.size() + 2, userCommonItemEventDao.getAllElements().size());
    }

    @Test
    @Transactional
    public void getAllSinceTimeTest() throws Exception {
        OffsetDateTime now = OffsetDateTime.now();
        Collection<UserCommonItemEvent> result = userCommonItemEventDao.getAllSinceTime(now, offset, limit);
        assertEquals(0, result.size());

        now = EntityFactory.ITEM_USAGE_START_DATE;
        result = userCommonItemEventDao.getAllSinceTime(now, offset, limit);
        assertEquals(3, result.size());
    }

    @Test
    @Transactional
    public void getAllByUserIdTest() throws Exception {
        Long userId = event1.getUser().getId();
        Collection<UserCommonItemEvent> result = userCommonItemEventDao.getAllByUserId(userId, offset, limit);
        assertEquals(1, result.size());
        assertTrue(result.contains(event1));
    }

    @Test
    @Transactional
    public void getAllByUserIdAndTimeTest() throws Exception {
        Long userId = event1.getUser().getId();
        OffsetDateTime now = OffsetDateTime.now();

        Collection<UserCommonItemEvent> result = userCommonItemEventDao.getAllByUserIdAndTime(userId, now, offset, limit);
        assertEquals(0, result.size());

        now = EntityFactory.ITEM_USAGE_START_DATE;
        result = userCommonItemEventDao.getAllByUserIdAndTime(userId, now, offset, limit);
        assertEquals(1, result.size());
    }

    @Test
    @Transactional
    public void getAllSinceTimeCount() {
        long count = userCommonItemEventDao.getAllSinceTimeCount(oldDate);
        assertEquals(3L, count); // 3L - number of records satisfying query conditions added in setUp
    }

    @Test
    @Transactional
    public void getAllByUserIdCount() {
        long userId = list.get(0).getUser().getId();
        long count = userCommonItemEventDao.getAllByUserIdCount(userId);
        assertEquals(1L, count); // 1L - number of records satisfying query conditions added in setUp
    }

    @Test
    @Transactional
    public void getAllByUserIdAndTimeCount() {
        long userId = list.get(0).getUser().getId();
        long count = userCommonItemEventDao.getAllByUserIdAndTimeCount(userId, oldDate);
        assertEquals(1L, count); // 1L - number of records satisfying query conditions added in setUp
    }
}
