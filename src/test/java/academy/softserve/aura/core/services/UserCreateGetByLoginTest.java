package academy.softserve.aura.core.services;

import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:service/servises_and_dao_context.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserCreateGetByLoginTest {

    @Autowired
    private UserService userService;

    private static User added = null;

    @Test
    public void _1createUserTest() {
        User user = EntityFactory.createUserWithCredentialsWithotDepartment();
        String realPass = user.getPassword();
        added = userService.create(user);
        String hashedPass = added.getPassword();
        Assert.assertNotEquals(realPass, hashedPass);
        System.out.println("Hashed pass: " + hashedPass);

    }

    @Test
    public void _2getUserByLoginTest() {
        User gotByLogin = userService.findByLogin(added.getLogin());

        Assert.assertEquals(added, gotByLogin);

    }
}
