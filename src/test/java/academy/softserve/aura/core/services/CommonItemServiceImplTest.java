package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.CommonItemDao;
import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.entity.CommonItem;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.services.impl.CommonItemServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.services.messaging.MessageSender;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class CommonItemServiceImplTest {

    @Mock
    private CommonItemDao commonItemDao;

    @Mock
    private ItemDao itemDao;

    @Mock
    private ItemLogService itemLogService;

    @Mock
    private DepartmentService departmentService;

    @Mock
    private MessageSender messageSender;

    @InjectMocks
    private CommonItemServiceImpl commonItemService;

    @Test
    public void findFewByIsWorking() throws Exception {
        commonItemService.findFewByIsWorking(0, 10, true);
        Mockito.verify(commonItemDao, Mockito.times(1)).findFewByIsWorking(0, 10, true);
    }

    @Test
    public void findFew() throws Exception {
        commonItemService.findFew(0, 10);
        Mockito.verify(commonItemDao, Mockito.times(1)).getFew(0, 10);
    }

    @Test
    public void getItemDepartments() throws Exception {
        commonItemService.getItemDepartments(1L);
        Mockito.verify(commonItemDao, Mockito.times(1)).getCommonItemDepartments(1L);
    }

    @Test
    public void getCommonItemsCount() throws Exception {
        commonItemService.getCommonItemsCount();
        Mockito.verify(commonItemDao, Mockito.times(1)).getRecordsCount();
    }

    @Test
    public void getCommonItemsFilteredByIsWorkingCount() throws Exception {
        commonItemService.getCommonItemsFilteredByIsWorkingCount(true);
        Mockito.verify(commonItemDao, Mockito.times(1)).getCommonItemsFilteredByIsWorkingCount(true);
    }

    @Test
    public void assignCommonItemToWarehouse() throws Exception {
        CommonItem complexCommonItem = EntityFactory.createComplexCommonItem();
        Mockito.when(commonItemService.assignCommonItemToWarehouse(complexCommonItem)).thenReturn(true);
        Mockito.when(itemLogService.createAndSaveSimpleItemLog(Mockito.anyString(), Mockito.any(),  Mockito.any(), Mockito.anyString()))
                .thenReturn(new ItemLog(complexCommonItem.getId().toString(), EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test"));

        commonItemService.assignCommonItemToWarehouse(complexCommonItem);
        Mockito.verify(commonItemDao, Mockito.times(1)).assignCommonItemToWarehouse(complexCommonItem);
        Assert.assertNull(complexCommonItem.getDepartment());
        Mockito.verify(messageSender, Mockito.times(complexCommonItem.getComponents().size() + 1)).sendMessage(Mockito.any());
    }

    @Test
    public void setOwnerToHierarchyTest() throws Exception {
        CommonItem commonItem = EntityFactory.createComplexCommonItem();
        Mockito.when(itemDao.getElementByID(commonItem.getId())).thenReturn(commonItem);
        Mockito.when(departmentService.findById(commonItem.getDepartment().getId())).thenReturn(commonItem.getDepartment());
        Mockito.when(itemLogService.createAndSaveSimpleItemLog(Mockito.anyString(), Mockito.any(),  Mockito.any(), Mockito.anyString()))
                .thenReturn(new ItemLog(commonItem.getId().toString(), EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test"));

        commonItemService.delete(commonItem.getId());
        Mockito.verify(departmentService, Mockito.times(commonItem.getComponents().size() + 1)).findById(Mockito.anyLong());
        Mockito.verify(itemLogService, Mockito.times(commonItem.getComponents().size() + 1)).createAndSaveSimpleItemLog(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
    }
}