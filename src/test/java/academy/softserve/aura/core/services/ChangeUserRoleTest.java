package academy.softserve.aura.core.services;


import academy.softserve.aura.core.dao.impl.DepartmentDaoImpl;
import academy.softserve.aura.core.dao.impl.UserDaoImpl;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraUserException;
import academy.softserve.aura.core.services.impl.UserServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class ChangeUserRoleTest {

    @Mock
    private UserDaoImpl userDao;

    @Mock
    DepartmentDaoImpl departmentDao;

    @InjectMocks
    private UserServiceImpl userService;

    private User entity = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private User activeAdmin1 = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private User passiveAdmin = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private List<User> users = new ArrayList<>();
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Before
    public void setUp() {
       userService.setPasswordEncoder(passwordEncoder);

        activeAdmin1 = EntityFactory.createUserWithCredentialsAndNewDepartment();
        activeAdmin1.setUserRole(UserRole.ADMIN);
        activeAdmin1.setActive(true);

        entity = EntityFactory.createUserWithCredentialsAndNewDepartment();
        entity.setUserRole(UserRole.USER);
        entity.setId(activeAdmin1.getId());
        entity.setActive(true);

        passiveAdmin = EntityFactory.createUserWithCredentialsAndNewDepartment();
        passiveAdmin.setUserRole(UserRole.ADMIN);
        passiveAdmin.setActive(false);

        users = new ArrayList<>();
        users.add(activeAdmin1);
        users.add(passiveAdmin);

        when(userDao.getElementByID(anyLong())).thenReturn(activeAdmin1);
        when(userDao.findByRoles(anyInt(), anyInt(), anyCollection())).thenReturn(users);
        when(userDao.getAllElements()).thenReturn(users);

        PowerMockito.mockStatic(Utils.class);

        PowerMockito.when(Utils.getUserWhoCalledMethodAuthorities()).thenReturn(new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            add(new SimpleGrantedAuthority("ROLE_MANAGER"));
        }});

    }

    @Test(expected = AuraUserException.class)
    public void changeRoleExceptionIfNoActive() {
        userService.update(entity);
        verify(userDao, never()).updateElement(entity);
    }

    @Test(expected = AuraUserException.class)
    public void changeRoleExceptionIfNoAdmin() {
        users.remove(1);
        userService.update(entity);
        verify(userDao, never()).updateElement(entity);
    }

    @Test
    public void changeRoleSuccess() {
        users.get(1).setActive(true);
        entity.setId(users.get(1).getId());
        userService.update(entity);
        verify(userDao, times(1)).updateElement(entity);
    }
}
