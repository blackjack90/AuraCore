package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.ItemDao;
import academy.softserve.aura.core.dao.PrivateItemDao;
import academy.softserve.aura.core.entity.ItemEventType;
import academy.softserve.aura.core.entity.ItemLog;
import academy.softserve.aura.core.entity.PrivateItem;
import academy.softserve.aura.core.services.impl.PrivateItemServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.services.messaging.MessageSender;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class PrivateItemServiceImplTest {

    @Mock
    private PrivateItemDao privateItemDao;

    @Mock
    private ItemLogService itemLogService;

    @Mock
    private MessageSender messageSender;

    @Mock
    private UserService userService;

    @Mock
    private ItemDao itemDao;

    @InjectMocks
    private PrivateItemServiceImpl privateItemService;

    @Test
    public void findFewByIsWorking() throws Exception {
        privateItemService.findFewByIsWorking(0, 10, true);
        Mockito.verify(privateItemDao, Mockito.times(1)).findFewByIsWorking(0, 10, true);
    }

    @Test
    public void findFew() throws Exception {
        privateItemService.findFew(0, 10);
        Mockito.verify(privateItemDao, Mockito.times(1)).getFew(0, 10);
    }

    @Test
    public void getItemUsers() throws Exception {
        privateItemService.getItemUsers(1L);
        Mockito.verify(privateItemDao, Mockito.times(1)).getPrivateItemUsers(1L);
    }

    @Test
    public void getPrivateItemsCount() throws Exception {
        privateItemService.getPrivateItemsCount();
        Mockito.verify(privateItemDao, Mockito.times(1)).getRecordsCount();
    }

    @Test
    public void getPrivateItemsFilteredByIsWorkingCount() throws Exception {
        privateItemService.getPrivateItemsFilteredByIsWorkingCount(true);
        Mockito.verify(privateItemDao, Mockito.times(1)).getPrivateItemsFilteredByIsWorkingCount(true);
    }

    @Test
    public void assignPrivateItemToWarehouse() throws Exception {
        PrivateItem privateItem = EntityFactory.createComplexPrivateItem();
        Mockito.when(privateItemService.assignPrivateItemToWarehouse(privateItem)).thenReturn(true);
        Mockito.when(itemLogService.createAndSaveSimpleItemLog(Mockito.anyString(), Mockito.any(),  Mockito.any(), Mockito.anyString()))
                .thenReturn(new ItemLog(privateItem.getId().toString(), EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test"));

        privateItemService.assignPrivateItemToWarehouse(privateItem);
        Mockito.verify(privateItemDao, Mockito.times(1)).assignPrivateItemToWarehouse(privateItem);
        Assert.assertNull(privateItem.getOwner());
        Mockito.verify(messageSender, Mockito.times(privateItem.getComponents().size() + 1)).sendMessage(Mockito.any());
    }

    @Test
    public void setOwnerToHierarchyTest() throws Exception {
        PrivateItem privateItem = EntityFactory.createComplexPrivateItem();
        Mockito.when(itemDao.getElementByID(privateItem.getId())).thenReturn(privateItem);
        Mockito.when(userService.findById(privateItem.getOwner().getId())).thenReturn(privateItem.getOwner());
        Mockito.when(itemLogService.createAndSaveSimpleItemLog(Mockito.anyString(), Mockito.any(),  Mockito.any(), Mockito.anyString()))
                .thenReturn(new ItemLog(privateItem.getId().toString(), EntityFactory.ITEM_USAGE_START_DATE, ItemEventType.OWNER_CHANGED, "test"));

        privateItemService.delete(privateItem.getId());
        Mockito.verify(userService, Mockito.times(privateItem.getComponents().size() + 1)).findById(Mockito.anyLong());
        Mockito.verify(itemLogService, Mockito.times(privateItem.getComponents().size() + 1)).createAndSaveSimpleItemLog(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
    }
}