package academy.softserve.aura.core.services;


import academy.softserve.aura.core.dao.impl.DepartmentDaoImpl;
import academy.softserve.aura.core.dao.impl.UserDaoImpl;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraUserException;
import academy.softserve.aura.core.services.impl.UserServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class SetManagerToDepartmentTest {

    @Mock
    private UserDaoImpl userDao;

    @Mock
    private
    DepartmentDaoImpl departmentDao;

    @InjectMocks
    private UserServiceImpl userService;

    private User entity = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private User manager = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private List<User> depUsers = new ArrayList<>();

    private Long depId = user.getDepartment().getId();

    @Before
    public void setUp() {
        user.setUserRole(UserRole.USER);
        entity.setUserRole(UserRole.USER);

        manager.getDepartment().setId(depId);
        manager.setUserRole(UserRole.MANAGER);

        depUsers.add(user);
        depUsers.add(manager);

        Mockito.when(departmentDao.getDepartmentUsers(depId)).thenReturn(new ArrayList<>(depUsers));
        Mockito.when(userDao.getElementByID(Mockito.anyLong())).thenReturn(user);


        PowerMockito.mockStatic(Utils.class);

        PowerMockito.when(Utils.getUserWhoCalledMethodAuthorities()).thenReturn(new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }});

        userService.setPasswordEncoder(new BCryptPasswordEncoder());
    }

    @Test(expected = AuraUserException.class)
    public void setManagerRoleFail() {
        entity.setDepartment(user.getDepartment());
        entity.setUserRole(UserRole.MANAGER);

        userService.update(entity);
        Mockito.verify(userDao, Mockito.never()).updateElement(entity);
    }

    @Test
    public void setManagerActiveSuccess() {
        // Success because there isn't active Manager in Department
        manager.setActive(false);

        entity.setDepartment(user.getDepartment());
        entity.setUserRole(UserRole.MANAGER);

        userService.update(entity);
        Mockito.verify(userDao, Mockito.times(1)).updateElement(entity);
    }

    @Test
    public void setManagerRoleSuccess() {
        // Success because there isn't Manager in Department
        manager.setUserRole(UserRole.ADMIN);

        entity.setDepartment(user.getDepartment());
        entity.setUserRole(UserRole.MANAGER);

        userService.update(entity);
        Mockito.verify(userDao, Mockito.times(1)).updateElement(entity);
    }
}
