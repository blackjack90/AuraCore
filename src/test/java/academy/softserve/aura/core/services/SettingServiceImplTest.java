package academy.softserve.aura.core.services;


import academy.softserve.aura.core.dao.SettingDao;
import academy.softserve.aura.core.entity.Setting;
import academy.softserve.aura.core.services.impl.SettingServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class SettingServiceImplTest {

    @Mock
    private SettingDao settingDao;

    @InjectMocks
    private SettingServiceImpl settingService;

    private List<Setting> entities = new ArrayList<>();
    private Setting entity = EntityFactory.createSetting();

    @Test
    public void createTest(){
        Mockito.when(settingDao.getElementByID(entity.getId())).thenReturn(entity);
        settingService.create(entity);
        Mockito.verify(settingDao, Mockito.times(1)).updateElement(entity);
    }

    @Test
    public void createAll() {
        settingService.createAll(entities);
        Mockito.verify(settingDao, Mockito.never()).addAll(entities);
    }

    @Test
    public void findAll() {
        settingService.findAll();
        Mockito.verify(settingDao, Mockito.times(1)).getAllElements();
    }

    @Test
    public void findFew() {
        settingService.findFew(1, 3);
        Mockito.verify(settingDao, Mockito.never()).getFew(1, 3);
    }

    @Test
    public void updateSetting() {
        Mockito.when(settingDao.getElementByID(entity.getId())).thenReturn(entity);
        settingService.update(entity);
        Mockito.verify(settingDao, Mockito.never()).updateElement(entity);
    }

    @Test
    public void delete() {
        Mockito.when(settingDao.getElementByID(entity.getId())).thenReturn(entity);
        settingService.delete(entity.getId());
        Mockito.verify(settingDao, Mockito.times(1)).updateElement(entity);
    }

}
