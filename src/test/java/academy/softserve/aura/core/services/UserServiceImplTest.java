package academy.softserve.aura.core.services;

import academy.softserve.aura.core.dao.impl.UserDaoImpl;
import academy.softserve.aura.core.entity.User;
import academy.softserve.aura.core.entity.UserRole;
import academy.softserve.aura.core.exceptions.AuraUserException;
import academy.softserve.aura.core.exceptions.AuraUserValidationException;
import academy.softserve.aura.core.services.impl.UserServiceImpl;
import academy.softserve.aura.core.services.impl.Utils;
import academy.softserve.aura.core.utils.EntityFactory;
import org.hibernate.HibernateException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Utils.class)
public class UserServiceImplTest {

    @Mock
    private UserDaoImpl userDao;

    @InjectMocks
    private UserServiceImpl userService;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private List<User> entities = new ArrayList<>();
    private List<UserRole> roles = new ArrayList<>();
    private User entity = EntityFactory.createUserWithCredentialsAndNewDepartment();
    private List<Long> departments = new ArrayList<>();

    @Before
    public void setEncoder() {
        PowerMockito.mockStatic(Utils.class);

        PowerMockito.when(Utils.getUserWhoCalledMethodAuthorities()).thenReturn(new ArrayList<SimpleGrantedAuthority>() {{
            add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }});

        userService.setPasswordEncoder(new BCryptPasswordEncoder());
    }

    @Test
    public void createTest() {
        userService.create(entity);
        Mockito.verify(userDao, Mockito.times(1)).addElement(entity);
        Mockito.verify(userDao, Mockito.never()).updateElement(entity);
    }

    @Test
    public void createUpdateTest() {
        Mockito.when(userDao.getElementByID(entity.getId())).thenReturn(entity);
        userService.create(entity);
        Mockito.verify(userDao, Mockito.times(1)).updateElement(entity);
        Mockito.verify(userDao, Mockito.never()).addElement(entity);
    }

    @Test
    public void nullPasswordTest() throws Exception {
        entity.setPassword(null);
        try {
            userService.create(entity);
        }
        catch (AuraUserValidationException ex) {
            Assert.assertEquals("Password must not be null", ex.getMessage());
        }
    }

    @Test
    public void nullRoleTest() throws Exception {
        entity.setUserRole(null);
        try {
            userService.create(entity);
        }
        catch (AuraUserValidationException ex) {
            Assert.assertEquals("User role must not be null", ex.getMessage());
        }
    }

    @Test(expected = AuraUserValidationException.class)
    public void throwAuraUserValidationExceptionWhenCreatingUser() {
        when(userDao.addElement(entity)).thenThrow(new ConstraintViolationException("Duplication",
                new SQLException("duplication"), "bad user login"));
        userService.create(entity);
    }

    @Test(expected = AuraUserException.class)
    public void throwAuraUserExceptionWhenCreatingUser() {
        when(userDao.addElement(entity)).thenThrow(new HibernateException("Login duplication", new SQLException("login duplication")));
        userService.create(entity);
    }

    @Test
    public void createAllTest() {
        userService.createAll(entities);
        Mockito.verify(userDao, Mockito.times(1)).addAll(entities);
    }

    @Test
    public void findByIdTest() {
        userService.findById(entity.getId());
        Mockito.verify(userDao, Mockito.times(1)).getElementByID(entity.getId());
    }

    @Test
    public void findAllTest() {
        userService.findAll();
        Mockito.verify(userDao, Mockito.times(1)).getAllElements();
    }

    @Test
    public void findFewTest() {
        userService.findFew(1, 3);
        Mockito.verify(userDao, Mockito.times(1)).getFew(1, 3);
    }

    @Test
    public void _01updateNonexistentUserTest() {
        Mockito.when(userDao.getElementByID(Mockito.anyLong())).thenReturn(null);
        userService.update(entity);
        Mockito.verify(userDao, Mockito.times(1)).addElement(entity);
        Mockito.verify(userDao, Mockito.never()).updateElement(entity);
    }

    @Test
    public void _02updateUserWithNoRoleTest() {
        Mockito.when(userDao.getElementByID(Mockito.anyLong())).thenReturn(EntityFactory.createUserWithCredentialsWithotDepartment());
        Mockito.when(userDao.updateElement(entity)).thenReturn(entity);
        entity.setUserRole(null);
        entity = userService.update(entity);
        Assert.assertNotNull(entity.getUserRole());
        Mockito.verify(userDao, Mockito.times(1)).updateElement(entity);
        Mockito.verify(userDao, Mockito.never()).addElement(entity);
    }

    @Test
    public void _03basicUpdateTest() {
        User user = new User();
        user.setLogin("Login");
        user.setPassword("password");
        user.setUserRole(UserRole.USER);
        user.setActive(true);
        when(userDao.getElementByID(Mockito.anyLong())).thenReturn(user);
        userService.update(entity);
        Mockito.verify(userDao, Mockito.times(1)).updateElement(entity);
    }

    @Test
    public void deleteTest() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        when(userDao.getElementByID(Mockito.anyLong())).thenReturn(user);
        userService.delete(entity.getId());
        Mockito.verify(userDao, Mockito.times(0)).deleteElement(entity.getId());
        Mockito.verify(userDao, Mockito.times(1)).updateElement(user);
    }

    @Test
    public void findByRolesTest() {
        userService.findByRoles(1, 3, roles);
        Mockito.verify(userDao, Mockito.times(1)).findByRoles(1, 3, roles);
    }

    @Test
    public void findByDepartmentsTest() {
        userService.findByDepartments(1, 3, departments);
        Mockito.verify(userDao, Mockito.times(1)).findByDepartments(1, 3, departments);
    }

    @Test
    public void findByDepartmentsAndRolesTest() {
        userService.findByDepartmentsAndRoles(1, 3, departments, roles);
        Mockito.verify(userDao, Mockito.times(1)).findByDepartmentsAndRoles(1, 3, departments, roles);
    }

    @Test
    public void getUserPrivateItemsTest() {
        userService.getUserPrivateItems(entity.getId());
        Mockito.verify(userDao, Mockito.times(1)).getUserPrivateItems(entity.getId());
    }

    @Test
    public void getUsersFilteredByDepartmentsCountTest() {
        userService.getUsersFilteredByDepartmentsCount(departments);
        Mockito.verify(userDao, Mockito.times(1))
                .getUsersFilteredByDepartmentsCount(departments);
    }

    @Test
    public void getUsersFilteredByDepartmentAndRolesCountTest() {
        userService.getUsersFilteredByDepartmentAndRolesCount(departments, roles);
        Mockito.verify(userDao, Mockito.times(1))
                .getUsersFilteredByDepartmentAndRolesCount(departments, roles);
    }

    @Test
    public void getUserPrivateItemsCountTest() {
        userService.getUserPrivateItemsCount(entity.getId());
        Mockito.verify(userDao, Mockito.times(1)).getUserPrivateItemsCount(entity.getId());
    }


    @Test
    public void getUsersFilteredByRolesCountTest() {
        userService.getUsersFilteredByRolesCount(roles);
        Mockito.verify(userDao, Mockito.times(1)).getUsersFilteredByRolesCount(roles);
    }
}
