#!/usr/bin/env bash

# Cleanup the database folders and persistence volumes
sudo rm -rf ./persistence
sudo docker volume prune -f