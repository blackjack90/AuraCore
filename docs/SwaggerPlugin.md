# Swagger Codegen

#### Table of content
[Wiki](wiki)

[Convention](#convention)

[Plugin setup](#plugin-setup)

[Workflow](#workflow)


## Wiki
**Swagger Codegen** is powerful tool for the multilingual generation which allows generation of API client libraries, server stubs and documentation automatically.
> Note: to generate your RESTful API you should create your API specification file first. Check [official docs](http://swagger.io/specification).

Visit [Swagger.io](http://swagger.io/docs/) for more info.

## Convention
1.	We use [Swaggen Codegen Maven plugin](https://github.com/swagger-api/swagger-codegen/tree/master/modules/swagger-codegen-maven-plugin) to generate server side API.
2.	We don't make any changes in generated sources, we don't move generated sources.
3.	We inherit/implement generated sources in case we need add some implementations.
4.	In case we need some API changes, we update API specification file and regenerate API.
5.	Path to the API specification file:
    ```
    {Project}/swagger/swagger.yaml
    ```
6. Path to the generated documentation:
    ```
    {Project}/target/generated-sources/swagger/docs/index.html
    ```


## Plugin setup
1.	Add [swagger-core](https://mvnrepository.com/artifact/io.swagger/swagger-core) and [springfox-swagger2](https://mvnrepository.com/artifact/io.springfox/springfox-swagger2) dependencies to Maven project
2.	Setup [Swagger Codegen Maven plugin](https://github.com/swagger-api/swagger-codegen/tree/master/modules/swagger-codegen-maven-plugin):
-	main execution (minimum configuration)
    ```xml
    <execution>
        <id>api</id>
        <goals>
            <goal>generate</goal>
        </goals>
        <configuration>
            <inputSpec>${swagger.yaml.file}</inputSpec>
            <language>spring-mvc</language>
            <configOptions>
                <library>j8-async</library>
            </configOptions>
        </configuration>
    </execution>
     ```
     Where:
     <inputSpec> - path to the API specification file;
     <language> - definition of target generation language;
     In <configOptions> we optionally added j8-async library (provides Java8 interfaces and Spring’s async servlet features).

-	execution for docs generation
    ```xml
    <execution>
        <id>docs</id>
        <goals>
            <goal>generate</goal>
        </goals>
        <configuration>
            <inputSpec>${swagger.yaml.file}</inputSpec>
            <language>dynamic-html</language>
            <output>target/generated-sources/swagger/</output>
        </configuration>
    </execution>
     ```
## Workflow
### Strat new project
1.	Add current API specification file
2.	[Setup](#Plugin-setup) Swagger Codegen plugin
3.	Generate your API using Maven command:
    ```
    mvn compile
    ```
4.	Check the results in output folder:
    ```
    {Project}\target\generated-sources\swagger\src\main\java\io\swagger
    ```
### Update/regenerate API
1.	Update/Pull current API specification file
3.	Update your API using Maven command:
    ```
    mvn clean compile
    ```
3.	Check the results in output folder.
> Note: don't forget check and refactor your classes inherited from generated API stubs in case you have made major updates and/or rename methods.